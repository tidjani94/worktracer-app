package net.coodiv.worktracer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.adapter.ProjectAdapter;
import net.coodiv.worktracer.helper.AuthManager;
import net.coodiv.worktracer.helper.RecyclerItemClickListener;
import net.coodiv.worktracer.model.Project;
import net.coodiv.worktracer.preferences.PrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProjectsListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    private PrefManager prefManager;
    private AuthManager authManager;
    private RequestQueue queue;
    private Gson gson;
    private ProjectAdapter mAdapter;
    private List<Project> projects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);
        gson = new Gson();

        Locale locale = new Locale("fr", "FR");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Tajawal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_basic_list);

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        recyclerView = findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, MotionEvent e) {
                setResult(RESULT_OK, new Intent().putExtra("project", gson.toJson(projects.get(position))));
                finish();
            }
        }));

        getProjects();
    }

    public void getProjects() {
        String url = getString(R.string.server_host_api) + "project?username=" + prefManager.getUsername()
                + "&token=" + prefManager.getSessionToken();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            projects = gson.fromJson(response, new TypeToken<List<Project>>() {
                            }.getType());
                        } catch (Exception exception) {
                            projects = new ArrayList<>();
                        }

                        Project.deleteAll(Project.class);
                        for (int i = 0; i < projects.size(); i++) {
                            projects.get(i).save();
                        }

                        mAdapter = new ProjectAdapter(ProjectsListActivity.this, projects);
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                projects = Project.listAll(Project.class);
                mAdapter = new ProjectAdapter(ProjectsListActivity.this, projects);
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });

        stringRequest.setShouldCache(false);

        queue.add(stringRequest);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
