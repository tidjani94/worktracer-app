package net.coodiv.worktracer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.helper.AuthManager;
import net.coodiv.worktracer.model.Employee;
import net.coodiv.worktracer.model.Project;
import net.coodiv.worktracer.model.Report;
import net.coodiv.worktracer.model.Site;
import net.coodiv.worktracer.model.Transactions;
import net.coodiv.worktracer.preferences.PrefManager;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SubmitReportActivity extends AppCompatActivity {

    private TextView progressionTxt, notes;
    private SeekBar progression;
    private Button save;

    private PrefManager prefManager;
    private AuthManager authManager;
    private RequestQueue queue;
    private Gson gson;
    private String date;
    private List<Employee> team;
    private Site mSite;
    private Project mProject;
    private int mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);
        gson = new Gson();

        Locale locale = new Locale("fr", "FR");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Tajawal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_submit_report);

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
            if (prefManager.getTeamLeader() == 0) {
                finish();
            }
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        team = gson.fromJson(getIntent().getStringExtra("team"), new TypeToken<List<Employee>>() {
        }.getType());
        mSite = gson.fromJson(getIntent().getStringExtra("site"), Site.class);
        mProject = gson.fromJson(getIntent().getStringExtra("project"), Project.class);
        date = getIntent().getStringExtra("date");
        mProgress = 0;

        progressionTxt = findViewById(R.id.progression_text);
        progression = findViewById(R.id.progression);
        notes = findViewById(R.id.notes);
        save = findViewById(R.id.save);

        progression.incrementProgressBy(10);
        progression.setProgress(0);
        progression.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress / 10;
                progress = progress * 10;
                mProgress = progress;
                progressionTxt.setText(String.valueOf(progress) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        List<Report> reports = Report.listAll(Report.class);

        if (reports.size() > 0) {
            progression.setProgress(reports.get(0).getProgression());
            notes.setText(reports.get(0).getNotes());
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mProgress == 0) {
                    Toast.makeText(SubmitReportActivity.this, R.string.progression_cant_be_zero, Toast.LENGTH_SHORT).show();
                } else if (notes.getText().toString().trim().length() == 0) {
                    Toast.makeText(SubmitReportActivity.this, R.string.please_enter_note, Toast.LENGTH_SHORT).show();
                } else {
                    saveReport();
                }
            }
        });

    }

    private void saveReport() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.wait_connect_dialog), true);

        String url = getString(R.string.server_host_api) + "report";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Report.deleteAll(Report.class);
                        setResult(RESULT_OK);
                        finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(SubmitReportActivity.this, R.string.cnx_prblm, Toast.LENGTH_SHORT).show();
                Report.deleteAll(Report.class);
                Report report = new Report(mSite.getId().intValue(), mProject.getId().intValue(), mProgress, notes.getText().toString().trim(), date, getIntent().getStringExtra("team"), mSite.getCode(), mProject.getName());
                report.save();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", prefManager.getUsername());
                params.put("token", prefManager.getSessionToken());
                params.put("site", String.valueOf(mSite.getId()));
                params.put("progression", String.valueOf(mProgress));
                params.put("date", date);
                params.put("team", gson.toJson(team));
                params.put("notes", notes.getText().toString().trim());

                return params;
            }
        };

        stringRequest.setShouldCache(false);

        queue.add(stringRequest);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
