package net.coodiv.worktracer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.helper.AuthManager;
import net.coodiv.worktracer.preferences.PrefManager;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReportCarActivity extends AppCompatActivity {

    private TextView img1, img2, img3, img4;
    private ImageView iv1, iv2, iv3, iv4;
    private EditText mileage;
    private Button save;

    private PrefManager prefManager;
    private AuthManager authManager;
    private RequestQueue queue;
    private Gson gson;
    private String imgStr1, imgStr2, imgStr3, imgStr4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);
        gson = new Gson();

        Locale locale = new Locale("fr", "FR");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Tajawal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_report_car);

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
            if (prefManager.getTeamLeader() == 0) {
                finish();
            }
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        imgStr1 = "";
        imgStr2 = "";
        imgStr3 = "";
        imgStr4 = "";

        save = findViewById(R.id.save);
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        img4 = findViewById(R.id.img4);
        iv1 = findViewById(R.id.iv1);
        iv2 = findViewById(R.id.iv2);
        iv3 = findViewById(R.id.iv3);
        iv4 = findViewById(R.id.iv4);
        mileage = findViewById(R.id.mileage);

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickImageDialog.build(
                        new PickSetup()
                                .setPickTypes(EPickType.CAMERA)
                                .setVideo(false)
                                .setTitle(getString(R.string.choose_camera))
                                .setCancelText(getString(R.string.cancel))
                )
                        .setOnPickResult(new IPickResult() {
                            @Override
                            public void onPickResult(PickResult pickResult) {
                                Picasso.get().load(pickResult.getUri()).into(iv1);
                                imgStr1 = getStringImage(pickResult.getBitmap());
                            }
                        })
                        .show(ReportCarActivity.this);
            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickImageDialog.build(
                        new PickSetup()
                                .setPickTypes(EPickType.CAMERA)
                                .setVideo(false)
                                .setTitle(getString(R.string.choose_camera))
                                .setCancelText(getString(R.string.cancel))
                )
                        .setOnPickResult(new IPickResult() {
                            @Override
                            public void onPickResult(PickResult pickResult) {
                                Picasso.get().load(pickResult.getUri()).into(iv2);
                                imgStr2 = getStringImage(pickResult.getBitmap());
                            }
                        })
                        .show(ReportCarActivity.this);
            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickImageDialog.build(
                        new PickSetup()
                                .setPickTypes(EPickType.CAMERA)
                                .setVideo(false)
                                .setTitle(getString(R.string.choose_camera))
                                .setCancelText(getString(R.string.cancel))
                )
                        .setOnPickResult(new IPickResult() {
                            @Override
                            public void onPickResult(PickResult pickResult) {
                                Picasso.get().load(pickResult.getUri()).into(iv3);
                                imgStr3 = getStringImage(pickResult.getBitmap());
                            }
                        })
                        .show(ReportCarActivity.this);
            }
        });

        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickImageDialog.build(
                        new PickSetup()
                                .setPickTypes(EPickType.CAMERA)
                                .setVideo(false)
                                .setTitle(getString(R.string.choose_camera))
                                .setCancelText(getString(R.string.cancel))
                )
                        .setOnPickResult(new IPickResult() {
                            @Override
                            public void onPickResult(PickResult pickResult) {
                                Picasso.get().load(pickResult.getUri()).into(iv4);
                                imgStr4 = getStringImage(pickResult.getBitmap());
                            }
                        })
                        .show(ReportCarActivity.this);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mileage.getText().toString().trim().length() == 0) {
                    Toast.makeText(ReportCarActivity.this, R.string.please_enter_mileage, Toast.LENGTH_SHORT).show();
                } else if (imgStr1.trim().length() == 0 || imgStr3.trim().length() == 0 || imgStr3.trim().length() == 0 || imgStr4.trim().length() == 0) {
                    Toast.makeText(ReportCarActivity.this, R.string.please_capture_all_images, Toast.LENGTH_SHORT).show();
                } else {
                    saveReport();
                }
            }
        });
    }

    private void saveReport() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.wait_connect_dialog), true);

        String url = getString(R.string.server_host_api) + "car-report";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        setResult(RESULT_OK);
                        finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", prefManager.getUsername());
                params.put("token", prefManager.getSessionToken());
                params.put("img1", imgStr1);
                params.put("img2", imgStr2);
                params.put("img3", imgStr3);
                params.put("img4", imgStr4);
                params.put("mileage", mileage.getText().toString().trim());

                return params;
            }
        };

        stringRequest.setShouldCache(false);

        queue.add(stringRequest);
    }

    public String getStringImage(Bitmap bmp) {
        if (bmp == null) {
            return "";
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}