package net.coodiv.worktracer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.helper.AuthManager;
import net.coodiv.worktracer.model.Employee;
import net.coodiv.worktracer.model.Project;
import net.coodiv.worktracer.model.Site;
import net.coodiv.worktracer.model.Transactions;
import net.coodiv.worktracer.preferences.PrefManager;

import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SubmitExpenditureActivity extends AppCompatActivity {

    private TextView addWarrant;
    private EditText amount;
    private ImageView warrant;
    private Spinner reason;
    private Button save;

    private PrefManager prefManager;
    private AuthManager authManager;
    private RequestQueue queue;
    private Gson gson;
    private String date;
    private List<Employee> team;
    private Site mSite;
    private Project mProject;
    private String warrantPic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);
        gson = new Gson();

        Locale locale = new Locale("fr", "FR");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Tajawal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_submit_expenditure);

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
            if (prefManager.getTeamLeader() == 0) {
                finish();
            }
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        team = gson.fromJson(getIntent().getStringExtra("team"), new TypeToken<List<Employee>>() {
        }.getType());
        mSite = gson.fromJson(getIntent().getStringExtra("site"), Site.class);
        mProject = gson.fromJson(getIntent().getStringExtra("project"), Project.class);
        date = getIntent().getStringExtra("date");
        warrantPic = "";

        addWarrant = findViewById(R.id.add_warrant);
        warrant = findViewById(R.id.warrant);
        reason = findViewById(R.id.reason);
        save = findViewById(R.id.save);
        amount = findViewById(R.id.amount);

        ArrayAdapter aa = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                getResources().getTextArray(R.array.expenditure_reason)
        );
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        reason.setAdapter(aa);

        List<Transactions> transactions = Transactions.listAll(Transactions.class);

        if (transactions.size() > 0) {
            reason.setSelection(transactions.get(0).getReasonPosition());
            amount.setText(String.valueOf(transactions.get(0).getAmount()));
        }

        addWarrant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickImageDialog.build(
                        new PickSetup()
                                .setPickTypes(EPickType.CAMERA)
                                .setVideo(false)
                                .setTitle(getString(R.string.choose_camera))
                                .setCancelText(getString(R.string.cancel))
                )
                        .setOnPickResult(new IPickResult() {
                            @Override
                            public void onPickResult(PickResult pickResult) {
                                Picasso.get().load(pickResult.getUri()).into(warrant);
                                warrantPic = getStringImage(pickResult.getBitmap());
                            }
                        })
                        .show(SubmitExpenditureActivity.this);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reason.getSelectedItemPosition() == 0) {
                    Toast.makeText(SubmitExpenditureActivity.this, R.string.please_select_expenditure_reason, Toast.LENGTH_SHORT).show();
                } else if (amount.getText().toString().trim().length() == 0) {
                    Toast.makeText(SubmitExpenditureActivity.this, R.string.please_enter_amount, Toast.LENGTH_SHORT).show();
                } else {
                    saveExpenditure();
                }
            }
        });

    }

    private void saveExpenditure() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.wait_connect_dialog), true);

        String url = getString(R.string.server_host_api) + "transaction";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Transactions.deleteAll(Transactions.class);
                        AlertDialog.Builder builder = new AlertDialog.Builder(SubmitExpenditureActivity.this);
                        builder.setTitle(getString(R.string.saved_expenditure))
                                .setMessage(getString(R.string.more_expenditure))
                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        setResult(RESULT_OK);
                                        startActivity(new Intent(SubmitExpenditureActivity.this, SubmitExpenditureActivity.class)
                                                .putExtras(getIntent().getExtras()));
                                        finish();
                                        dialog.dismiss();
                                    }
                                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                setResult(RESULT_OK);
                                finish();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(SubmitExpenditureActivity.this, R.string.cnx_prblm, Toast.LENGTH_SHORT).show();
                Transactions.deleteAll(Transactions.class);
                Transactions transactions = new Transactions(Double.parseDouble(amount.getText().toString().trim()), mSite.getId().intValue(), mProject.getId().intValue(), date, getIntent().getStringExtra("team"), mSite.getCode(), mProject.getName(), reason.getSelectedItem().toString(), reason.getSelectedItemPosition());
                transactions.save();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", prefManager.getUsername());
                params.put("token", prefManager.getSessionToken());
                params.put("site", String.valueOf(mSite.getId()));
                params.put("date", date);
                params.put("team", gson.toJson(team));
                params.put("warrant", warrantPic);
                params.put("reason", reason.getSelectedItem().toString());
                params.put("amount", amount.getText().toString().trim());

                return params;
            }
        };

        stringRequest.setShouldCache(false);

        queue.add(stringRequest);
    }

    public String getStringImage(Bitmap bmp) {
        if (bmp == null) {
            return "";
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
