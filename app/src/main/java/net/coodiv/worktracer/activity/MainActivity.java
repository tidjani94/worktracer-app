package net.coodiv.worktracer.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.helper.AuthManager;
import net.coodiv.worktracer.preferences.PrefManager;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private LinearLayout expenditureReport, positionSharing, workReport, carReport;
    private ImageView profile, about;
    private View separator;

    private PrefManager prefManager;
    private AuthManager authManager;
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);

        Locale locale = new Locale("fr", "FR");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Tajawal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_main);

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        expenditureReport = findViewById(R.id.expenditure_report);
        positionSharing = findViewById(R.id.position_sharing);
        workReport = findViewById(R.id.work_report);
        carReport = findViewById(R.id.car_report);
        separator = findViewById(R.id.separator);
        profile = findViewById(R.id.profile);
        about = findViewById(R.id.about);

        if (prefManager.getTeamLeader() == 0) {
            expenditureReport.setVisibility(View.GONE);
            separator.setVisibility(View.GONE);
        } else {
            expenditureReport.setVisibility(View.VISIBLE);
            separator.setVisibility(View.VISIBLE);
        }

        expenditureReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ExpenditureActivity.class));
            }
        });

        positionSharing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SharePositionActivity.class));
            }
        });

        workReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AddReportActivity.class));
            }
        });

        carReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ReportCarActivity.class));
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AboutActivity.class));
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
