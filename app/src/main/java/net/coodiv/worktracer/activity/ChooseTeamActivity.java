package net.coodiv.worktracer.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.adapter.EmployeeAdapter;
import net.coodiv.worktracer.adapter.ProjectAdapter;
import net.coodiv.worktracer.helper.AuthManager;
import net.coodiv.worktracer.helper.RecyclerItemClickListener;
import net.coodiv.worktracer.model.Employee;
import net.coodiv.worktracer.model.Project;
import net.coodiv.worktracer.preferences.PrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChooseTeamActivity extends AppCompatActivity {

    private RecyclerView colleaguesRecyclerView, teamRecyclerView;
    private ProgressBar progress;

    private PrefManager prefManager;
    private AuthManager authManager;
    private RequestQueue queue;
    private Gson gson;
    private List<Employee> employees, team;
    private EmployeeAdapter colleaguesAdapter, teamAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);
        gson = new Gson();

        Locale locale = new Locale("fr", "FR");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Tajawal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_choose_team);

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
            if (prefManager.getTeamLeader() == 0) {
                finish();
            }
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        colleaguesRecyclerView = findViewById(R.id.colleagues_recycler_view);
        teamRecyclerView = findViewById(R.id.team_recycler_view);
        progress = findViewById(R.id.progress);

        RecyclerView.LayoutManager colleaguesLayoutManager = new GridLayoutManager(this, 3);
        colleaguesRecyclerView.setLayoutManager(colleaguesLayoutManager);
        colleaguesRecyclerView.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.LayoutManager teamLayoutManager = new GridLayoutManager(this, 3);
        teamRecyclerView.setLayoutManager(teamLayoutManager);
        teamRecyclerView.setItemAnimator(new DefaultItemAnimator());
        team = new ArrayList<>();
        teamAdapter = new EmployeeAdapter(team, ChooseTeamActivity.this);
        teamRecyclerView.setAdapter(teamAdapter);
        teamAdapter.notifyDataSetChanged();

        colleaguesRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, MotionEvent e) {
                team.add(employees.get(position));
                employees.remove(position);
                colleaguesAdapter.notifyDataSetChanged();
                teamAdapter.notifyDataSetChanged();
                setResult(RESULT_OK, new Intent().putExtra("team", gson.toJson(team)));
            }
        }));

        teamRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, MotionEvent e) {
                employees.add(team.get(position));
                team.remove(position);
                teamAdapter.notifyDataSetChanged();
                colleaguesAdapter.notifyDataSetChanged();
                setResult(RESULT_OK, new Intent().putExtra("team", gson.toJson(team)));
            }
        }));

        getColleagues();
    }

    private void getColleagues() {
        progress.setVisibility(View.VISIBLE);
        colleaguesRecyclerView.setVisibility(View.GONE);

        String url = getString(R.string.server_host_api) + "team?username=" + prefManager.getUsername()
                + "&token=" + prefManager.getSessionToken();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.setVisibility(View.GONE);

                        try {
                            employees = gson.fromJson(response, new TypeToken<List<Employee>>() {
                            }.getType());
                        } catch (Exception exception) {
                            employees = new ArrayList<>();
                        }

                        if (employees.size() == 0) {
                            colleaguesRecyclerView.setVisibility(View.GONE);
                        } else {
                            colleaguesRecyclerView.setVisibility(View.VISIBLE);
                        }

                        Employee.deleteAll(Employee.class);
                        for (int i = 0; i < employees.size(); i++) {
                            employees.get(i).save();
                        }

                        colleaguesAdapter = new EmployeeAdapter(employees, ChooseTeamActivity.this);
                        colleaguesRecyclerView.setAdapter(colleaguesAdapter);
                        colleaguesAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                colleaguesRecyclerView.setVisibility(View.VISIBLE);
                employees = Employee.listAll(Employee.class);
                colleaguesAdapter = new EmployeeAdapter(employees, ChooseTeamActivity.this);
                colleaguesRecyclerView.setAdapter(colleaguesAdapter);
                colleaguesAdapter.notifyDataSetChanged();
            }
        });

        stringRequest.setShouldCache(false);

        queue.add(stringRequest);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
