package net.coodiv.worktracer.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.adapter.TransactionAdapter;
import net.coodiv.worktracer.helper.AuthManager;
import net.coodiv.worktracer.helper.RecyclerItemClickListener;
import net.coodiv.worktracer.model.Transactions;
import net.coodiv.worktracer.preferences.PrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ExpenditureActivity extends AppCompatActivity {

    private static final int ADD_EXPENDITURE_REQUEST_CODE = 1;
    private TextView balance;
    private RecyclerView recyclerView;
    private ProgressBar progress;
    private ImageView addExpenditure, transactionsList;

    private PrefManager prefManager;
    private AuthManager authManager;
    private RequestQueue queue;
    private List<Transactions> transactions;
    private TransactionAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);

        Locale locale = new Locale("fr", "FR");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Tajawal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_expenditure);

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
            if (prefManager.getTeamLeader() == 0) {
                finish();
            }
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        balance = findViewById(R.id.balance);
        recyclerView = findViewById(R.id.recycler_view);
        progress = findViewById(R.id.progress);
        addExpenditure = findViewById(R.id.add_expenditure);
        transactionsList = findViewById(R.id.transactions);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, MotionEvent e) {

            }
        }));

        addExpenditure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(ExpenditureActivity.this, AddExpenditureActivity.class)
                        , ADD_EXPENDITURE_REQUEST_CODE);
            }
        });

        transactionsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTransactions();
            }
        });

        getBalance();
        getTransactions();
    }

    private void getBalance() {
        String url = getString(R.string.server_host_api) + "balance?username=" + prefManager.getUsername()
                + "&token=" + prefManager.getSessionToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Gson gson = new Gson();
                        double amount;
                        try {
                            amount = Double.parseDouble(gson.fromJson(response.trim(), String.class));
                        } catch (Exception e) {
                            amount = 0;
                        }

                        balance.setText(String.valueOf(amount) + "DZ");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        queue.add(stringRequest);
    }

    private void getTransactions() {
        progress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        String url = getString(R.string.server_host_api) + "transaction?username=" + prefManager.getUsername()
                + "&token=" + prefManager.getSessionToken();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.setVisibility(View.GONE);
                        Gson gson = new Gson();

                        try {
                            transactions = gson.fromJson(response, new TypeToken<List<Transactions>>() {
                            }.getType());
                        } catch (Exception exception) {
                            transactions = new ArrayList<>();
                        }

                        if (transactions.size() == 0) {
                            //todo show empty transactions
                            recyclerView.setVisibility(View.GONE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                        }

                        mAdapter = new TransactionAdapter(transactions, ExpenditureActivity.this);
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
            }
        });

        stringRequest.setShouldCache(false);

        queue.add(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_EXPENDITURE_REQUEST_CODE && resultCode == RESULT_OK) {
            getBalance();
            getTransactions();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
