package net.coodiv.worktracer.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.helper.AuthManager;
import net.coodiv.worktracer.model.Employee;
import net.coodiv.worktracer.model.Project;
import net.coodiv.worktracer.model.Site;
import net.coodiv.worktracer.model.Transactions;
import net.coodiv.worktracer.preferences.PrefManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddExpenditureActivity extends AppCompatActivity {

    private static final int PROJECT_REQUEST_CODE = 1;
    private static final int SITE_REQUEST_CODE = 2;
    private static final int CHOOSE_TEAM_REQUEST_CODE = 3;
    private static final int SUBMIT_REPORT_REQUEST_CODE = 4;

    private TextView project, site, date, team;
    private Button next;

    private PrefManager prefManager;
    private AuthManager authManager;
    private RequestQueue queue;
    private Gson gson;
    private DatePickerDialog datePickerDialog;
    private List<Employee> teamList;
    private Project mProject;
    private Site mSite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);
        gson = new Gson();

        Locale locale = new Locale("fr", "FR");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Tajawal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_add_expenditure);

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
            if (prefManager.getTeamLeader() == 0) {
                finish();
            }
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        project = findViewById(R.id.project);
        site = findViewById(R.id.site);
        next = findViewById(R.id.next);
        date = findViewById(R.id.date);
        team = findViewById(R.id.team);

        teamList = new ArrayList<>();

        project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(AddExpenditureActivity.this, ProjectsListActivity.class)
                        , PROJECT_REQUEST_CODE);
            }
        });

        site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mProject == null) {
                    Toast.makeText(AddExpenditureActivity.this, R.string.pls_select_project, Toast.LENGTH_SHORT).show();
                } else {
                    startActivityForResult(new Intent(AddExpenditureActivity.this, SitesListActivity.class)
                                    .putExtra("project", mProject.getId().intValue())
                            , SITE_REQUEST_CODE);
                }
            }
        });

        List<Transactions> transactions = Transactions.listAll(Transactions.class);

        if (transactions.size() > 0) {
            date.setText(transactions.get(0).getDate());
            project.setText(transactions.get(0).getProjectName());
            mProject = new Project((long) transactions.get(0).getProject(), transactions.get(0).getProjectName());
            mSite = new Site((long) transactions.get(0).getSite(), transactions.get(0).getSiteCode());
            site.setText(transactions.get(0).getSiteCode());
            teamList = gson.fromJson(transactions.get(0).getTeam(), new TypeToken<List<Employee>>() {
            }.getType());
            if (teamList.size() != 0) {
                String teamNames = "";
                for (int i = 0; i < teamList.size(); i++) {
                    teamNames += "'" + teamList.get(i).getName() + "' ";
                }
                team.setText(teamNames);
            }
        }

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mProject == null) {
                    Toast.makeText(AddExpenditureActivity.this, R.string.please_select_project_site, Toast.LENGTH_SHORT).show();
                } else if (mSite == null) {
                    Toast.makeText(AddExpenditureActivity.this, R.string.please_select_site, Toast.LENGTH_SHORT).show();
                } else if (date.getText().toString().trim().length() == 0) {
                    Toast.makeText(AddExpenditureActivity.this, R.string.please_enter_date, Toast.LENGTH_SHORT).show();
                } else {
                    startActivityForResult(new Intent(AddExpenditureActivity.this, SubmitExpenditureActivity.class)
                                    .putExtra("site", gson.toJson(mSite))
                                    .putExtra("project", gson.toJson(mProject))
                                    .putExtra("date", date.getText().toString().trim())
                                    .putExtra("team", gson.toJson(teamList))
                            , SUBMIT_REPORT_REQUEST_CODE);
                }
            }
        });

        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int year = Calendar.getInstance().get(Calendar.YEAR);
        datePickerDialog = new DatePickerDialog(
                this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                date.setText(i + "-" + (i1 + 1) + "-" + i2);
            }
        }, year, month, day);

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });

        team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(AddExpenditureActivity.this, ChooseTeamActivity.class)
                        , CHOOSE_TEAM_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PROJECT_REQUEST_CODE && resultCode == RESULT_OK) {
            mProject = gson.fromJson(data.getStringExtra("project"), Project.class);
            project.setText(mProject.getName());
        }

        if (requestCode == SITE_REQUEST_CODE && resultCode == RESULT_OK) {
            mSite = gson.fromJson(data.getStringExtra("site"), Site.class);
            site.setText(mSite.getCode());
        }

        if (requestCode == CHOOSE_TEAM_REQUEST_CODE && resultCode == RESULT_OK) {
            teamList = gson.fromJson(data.getStringExtra("team"), new TypeToken<List<Employee>>() {
            }.getType());
            if (teamList.size() != 0) {
                String teamNames = "";
                for (int i = 0; i < teamList.size(); i++) {
                    teamNames += "'" + teamList.get(i).getName() + "' ";
                }
                team.setText(teamNames);
            }
        }

        if (requestCode == SUBMIT_REPORT_REQUEST_CODE && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
