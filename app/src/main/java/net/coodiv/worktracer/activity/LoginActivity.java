package net.coodiv.worktracer.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.model.Employee;
import net.coodiv.worktracer.preferences.PrefManager;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {

    private EditText username, password;
    private Button logInBtn;
    private ProgressDialog connectDialog;

    private PrefManager prefManager;
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        queue = Volley.newRequestQueue(this);

        Locale locale = new Locale("fr", "FR");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Tajawal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_login);

        if (prefManager.isLoggedIn()) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        username = findViewById(R.id.login_username);
        password = findViewById(R.id.login_password);
        logInBtn = findViewById(R.id.login_btn);

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String result = username.getText().toString().replaceAll(" ", "");
                if (!username.getText().toString().equals(result)) {
                    username.setText(result);
                    username.setSelection(result.length());
                    // alert the user
                }
            }
        });

        if (getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            username.setGravity(Gravity.RIGHT);
            password.setGravity(Gravity.RIGHT);
        }

        logInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().trim().length() < 4) {
                    username.setError(getString(R.string.short_username));
                } else if (password.getText().toString().trim().length() < 8) {
                    password.setError(getString(R.string.short_password));
                } else {
                    logIn(username.getText().toString().trim(), password.getText().toString().trim());
                }
            }
        });
    }

    public void logIn(final String username, final String password) {
        connectDialog = ProgressDialog.show(this, null, getString(R.string.wait_connect_dialog), true);

        String url = getString(R.string.server_host_api) + "login?username=" + username
                + "&password=" + password;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        connectDialog.dismiss();

                        Gson gson = new Gson();
                        Employee employee = gson.fromJson(response.trim(), Employee.class);
                        prefManager.setSessionToken(employee.getToken().trim());
                        prefManager.setLoggedIn(true);
                        prefManager.setName(employee.getName());
                        prefManager.setUsername(employee.getUsername());
                        prefManager.setPhone(employee.getPhone());
                        prefManager.setTeamLeader(employee.getTeamLeader());

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                connectDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    switch (networkResponse.statusCode) {
                        case HttpURLConnection.HTTP_UNAUTHORIZED: {
                            Toast.makeText(LoginActivity.this, getString(R.string.username_or_password_error), Toast.LENGTH_LONG).show();
                            break;
                        }
                        case HttpURLConnection.HTTP_FORBIDDEN: {
                            Toast.makeText(LoginActivity.this, getString(R.string.blocked_account), Toast.LENGTH_LONG).show();
                            break;
                        }
                        default: {
                            Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                }
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        stringRequest.setShouldCache(false);

        queue.add(stringRequest);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
