package net.coodiv.worktracer.model;

import com.orm.SugarRecord;

public class Report extends SugarRecord {
    private int site;
    private int project;
    private int progression;
    private String notes;
    private String date;
    private String team;
    private String siteCode;
    private String projectName;

    public Report() {
    }

    public Report(int site, int project, int progression, String notes, String date, String team, String siteCode, String projectName) {
        this.site = site;
        this.project = project;
        this.progression = progression;
        this.notes = notes;
        this.date = date;
        this.team = team;
        this.siteCode = siteCode;
        this.projectName = projectName;
    }

    public int getProject() {
        return project;
    }

    public void setProject(int project) {
        this.project = project;
    }

    public int getSite() {
        return site;
    }

    public void setSite(int site) {
        this.site = site;
    }

    public int getProgression() {
        return progression;
    }

    public void setProgression(int progression) {
        this.progression = progression;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
