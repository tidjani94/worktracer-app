package net.coodiv.worktracer.model;

import com.orm.SugarRecord;

public class Site extends SugarRecord {
    private String code;
    private int project;

    public Site() {
    }

    public Site(String code, int project) {
        this.code = code;
        this.project = project;
    }

    public Site(long id, String code) {
        super.setId(id);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getProject() {
        return project;
    }

    public void setProject(int project) {
        this.project = project;
    }
}
