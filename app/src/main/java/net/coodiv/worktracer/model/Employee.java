package net.coodiv.worktracer.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Employee extends SugarRecord {
    private String name;
    private String username;
    private String phone;
    private String token;
    @SerializedName("team_leader")
    private int teamLeader;

    public Employee() {
    }

    public Employee(String name, String username, String phone, String token, int teamLeader) {
        this.name = name;
        this.username = username;
        this.phone = phone;
        this.token = token;
        this.teamLeader = teamLeader;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getTeamLeader() {
        return teamLeader;
    }

    public void setTeamLeader(int teamLeader) {
        this.teamLeader = teamLeader;
    }
}
