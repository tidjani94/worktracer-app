package net.coodiv.worktracer.model;

import com.google.gson.annotations.SerializedName;

public class EmployeeCar {
    private int id;
    private Car car;
    private int mileage;
    @SerializedName("assignment_date")
    private String assignmentDate;
    @SerializedName("final_mileage")
    private String finalMileage;

    public EmployeeCar() {
    }

    public EmployeeCar(int id, Car car, int mileage, String assignmentDate, String finalMileage) {
        this.id = id;
        this.car = car;
        this.mileage = mileage;
        this.assignmentDate = assignmentDate;
        this.finalMileage = finalMileage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(String assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public String getFinalMileage() {
        return finalMileage;
    }

    public void setFinalMileage(String finalMileage) {
        this.finalMileage = finalMileage;
    }
}
