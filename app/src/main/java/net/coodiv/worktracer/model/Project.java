package net.coodiv.worktracer.model;

import com.orm.SugarRecord;

public class Project extends SugarRecord {
    private String name;

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Project(Long id, String name) {
        super.setId(id);
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
