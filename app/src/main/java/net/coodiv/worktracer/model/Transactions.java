package net.coodiv.worktracer.model;

import com.orm.SugarRecord;

import java.util.List;

public class Transactions extends SugarRecord {
    private double amount;
    private int site;
    private int project;
    private String date;
    private String team;
    private String siteCode;
    private String projectName;
    private String reason;
    private int reasonPosition;

    public Transactions() {
    }

    public Transactions(double amount, int site, int project, String date, String team, String siteCode, String projectName, String reason, int reasonPosition) {
        this.amount = amount;
        this.site = site;
        this.project = project;
        this.date = date;
        this.team = team;
        this.siteCode = siteCode;
        this.projectName = projectName;
        this.reason = reason;
        this.reasonPosition = reasonPosition;
    }

    public int getSite() {
        return site;
    }

    public void setSite(int site) {
        this.site = site;
    }

    public int getProject() {
        return project;
    }

    public void setProject(int project) {
        this.project = project;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getReasonPosition() {
        return reasonPosition;
    }

    public void setReasonPosition(int reasonPosition) {
        this.reasonPosition = reasonPosition;
    }
}
