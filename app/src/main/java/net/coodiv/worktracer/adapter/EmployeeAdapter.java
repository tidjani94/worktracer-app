package net.coodiv.worktracer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.model.Employee;
import net.coodiv.worktracer.preferences.PrefManager;

import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.MyViewHolder> {

    private List<Employee> employees;
    private Context mContext;
    private PrefManager prefManager;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.employee_name);
            image = view.findViewById(R.id.employee_image);
        }
    }


    public EmployeeAdapter(List<Employee> employees, Context mContext) {
        this.mContext = mContext;
        this.prefManager = new PrefManager(mContext);
        this.employees = employees;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_empolyee, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Employee employee = employees.get(position);
        holder.name.setText(employee.getName());
        Picasso.get().load(mContext.getString(R.string.server_host_images)
                        + "employees/" + employee.getId() + ".jpg")
                .placeholder(R.drawable.user_placeholder)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return employees.size();
    }
}
