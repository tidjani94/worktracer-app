package net.coodiv.worktracer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.model.Project;
import net.coodiv.worktracer.model.Site;

import java.util.List;

public class SiteAdapter extends RecyclerView.Adapter<SiteAdapter.MyViewHolder> {

    private Context mContext;
    private List<Site> siteList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
        }
    }


    public SiteAdapter(Context mContext, List<Site> siteList) {
        this.mContext = mContext;
        this.siteList = siteList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_basic_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Site site = siteList.get(position);
        holder.name.setText(site.getCode());
    }

    @Override
    public int getItemCount() {
        return siteList.size();
    }
}