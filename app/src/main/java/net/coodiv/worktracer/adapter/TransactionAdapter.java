package net.coodiv.worktracer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.model.Transactions;
import net.coodiv.worktracer.preferences.PrefManager;

import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {

    private List<Transactions> transactions;
    private Context mContext;
    private PrefManager prefManager;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date, amount;
        public View deposit, payment;

        public MyViewHolder(View view) {
            super(view);
            date = view.findViewById(R.id.date);
            amount = view.findViewById(R.id.amount);
            deposit = view.findViewById(R.id.deposit);
            payment = view.findViewById(R.id.payment);
        }
    }


    public TransactionAdapter(List<Transactions> transactions, Context mContext) {
        this.mContext = mContext;
        this.prefManager = new PrefManager(mContext);
        this.transactions = transactions;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_transaction, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Transactions transaction = transactions.get(position);
        holder.date.setText(transaction.getDate());

        if (transaction.getAmount() >= 0) {
            holder.deposit.setVisibility(View.VISIBLE);
            holder.payment.setVisibility(View.GONE);
            holder.amount.setText(String.valueOf("+" + transaction.getAmount()));
        } else {
            holder.deposit.setVisibility(View.GONE);
            holder.payment.setVisibility(View.VISIBLE);
            holder.amount.setText(String.valueOf(transaction.getAmount()));
        }
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }
}
