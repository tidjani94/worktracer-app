package net.coodiv.worktracer.preferences;

import android.content.Context;
import android.content.SharedPreferences;


public class PrefManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    // shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file title
    private static final String PREF_NAME = "work_tracer";

    private static final String IS_LOGGED_IN = "is_logged_in";
    private static final String SESSION_TOKEN = "sessionToken";
    private static final String USERNAME = "username";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PHONE = "phone";
    private static final String TEAM_LEADER = "team_leader";

    public PrefManager(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLoggedIn(boolean isFirstTime) {
        editor.putBoolean(IS_LOGGED_IN, isFirstTime);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGGED_IN, false);
    }

    public void setSessionToken(String sessionToken) {
        editor.putString(SESSION_TOKEN, sessionToken);
        editor.commit();
    }

    public String getSessionToken() {
        return pref.getString(SESSION_TOKEN, null);
    }

    public void setUsername(String username) {
        editor.putString(USERNAME, username);
        editor.commit();
    }

    public String getUsername() {
        return pref.getString(USERNAME, null);
    }

    public void setId(int id) {
        editor.putInt(ID, id);
        editor.commit();
    }

    public int getId() {
        return pref.getInt(ID, 0);
    }

    public void setName(String name) {
        editor.putString(NAME, name);
        editor.commit();
    }

    public String getName() {
        return pref.getString(NAME, null);
    }

    public void setPhone(String phone) {
        editor.putString(PHONE, phone);
        editor.commit();
    }

    public String getPhone() {
        return pref.getString(PHONE, null);
    }

    public void setTeamLeader(int phone) {
        editor.putInt(TEAM_LEADER, phone);
        editor.commit();
    }

    public int getTeamLeader() {
        return pref.getInt(TEAM_LEADER, 0);
    }
}