package net.coodiv.worktracer.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import net.coodiv.worktracer.R;
import net.coodiv.worktracer.activity.LoginActivity;
import net.coodiv.worktracer.model.Employee;
import net.coodiv.worktracer.preferences.PrefManager;

import java.net.HttpURLConnection;

/**
 * Created by Ahmed Tidjani on 06-Aug-17.
 */

public class AuthManager {

    private Context mContext;
    private Activity activity;
    private String url;
    private PrefManager prefManager;
    private Employee employee;
    private Gson gson;
    private RequestQueue queue;

    public AuthManager(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.prefManager = new PrefManager(mContext);
        this.activity = activity;
        this.gson = new Gson();
        this.queue = Volley.newRequestQueue(mContext);
    }

    public void isLoggedIn() {

        this.url = mContext.getString(R.string.server_host_api)
                + "employee?username=" + prefManager.getUsername()
                + "&token=" + prefManager.getSessionToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        employee = gson.fromJson(response.trim(), Employee.class);
                        prefManager.setSessionToken(employee.getToken().trim());
                        prefManager.setLoggedIn(true);
                        prefManager.setId(employee.getId().intValue());
                        prefManager.setName(employee.getName());
                        prefManager.setUsername(employee.getUsername());
                        prefManager.setPhone(employee.getPhone());
                        prefManager.setTeamLeader(employee.getTeamLeader());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    switch (networkResponse.statusCode) {
                        case HttpURLConnection.HTTP_UNAUTHORIZED:
                            logout();
                            break;
                        case HttpURLConnection.HTTP_FORBIDDEN:
                            logout();
                            break;
                        default:
                            break;
                    }
                }
            }
        });

        stringRequest.setShouldCache(false);

        queue.add(stringRequest);
    }

    public void logout() {
        prefManager.setSessionToken(null);
        prefManager.setLoggedIn(false);
        prefManager.setUsername(null);
        prefManager.setName(null);
        prefManager.setPhone(null);
        prefManager.setTeamLeader(0);
        prefManager.setId(0);

        mContext.startActivity(new Intent(mContext, LoginActivity.class));
        activity.finish();
    }
}
